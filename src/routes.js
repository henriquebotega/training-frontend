import React from "react";
import { Redirect, BrowserRouter, Route, Switch } from "react-router-dom";
import Header from "./components/Header";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import Admin from "./pages/Admin";
import Level from "./pages/Level";
import Category from "./pages/Category";
import { useAuth } from "./contexts/auth";

const PublicRoute = () => {
	return (
		<>
			<Route path="/" component={Login} exact />
			<Route path="*" render={() => <Redirect to="/" />} />
		</>
	);
};

const PrivateRoute = () => {
	return (
		<>
			<Route path="/" component={Dashboard} exact />
			<Route path="/admin" component={Admin} />
			<Route path="/level/:id" component={Level} />
			<Route path="/category/:id" component={Category} />
			{/* <Route path="*" render={() => <Redirect to="/" />} /> */}
		</>
	);
};

const Routes = () => {
	const { signed, loading } = useAuth();

	if (loading) {
		return <div className="loading">Loading...</div>;
	}

	return (
		<BrowserRouter>
			{signed ? <Header /> : null}
			<Switch>{signed ? <PrivateRoute /> : <PublicRoute />}</Switch>
		</BrowserRouter>
	);
};

export default Routes;
