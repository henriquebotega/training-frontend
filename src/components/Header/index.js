import React from "react";
import { useHistory } from "react-router-dom";
import "./styles.css";
import { useAuth } from "../../contexts/auth";

const Header = () => {
	const history = useHistory();
	const { signed, logout, user } = useAuth();

	const goTo = (page) => {
		history.replace(page);
	};

	const sair = () => {
		logout();
		goTo("/");
	};

	return (
		<div className="contentHeader">
			<h2>{user?.nome || user?.name}</h2>

			<button onClick={() => goTo("/")}>Dashboard</button>

			{user.admin && (
				<>
					<button onClick={() => goTo("/admin")}>Admin</button>
				</>
			)}

			{signed && (
				<>
					<button onClick={() => sair()}>Logout</button>
				</>
			)}
		</div>
	);
};

export default Header;
