import React, { useState, useEffect } from "react";
import api from "../../services/api";
import { useHistory } from "react-router-dom";
import "./styles.css";
import mapa from "../../assets/images/map.jpg";
import seta from "../../assets/images/seta.png";
import { useReport } from "../../contexts/report";
import useFetch from "../../util/useFetch";

const Mapa = () => {
	const [items, setItems] = useState([]);
	const [lastLevel, setLastLevel] = useState(1);
	const [loading, setLoading] = useState(true);
	const { report, getInfoLevel } = useReport();
	const history = useHistory();

	const { response, loading: loadingFetch, error } = useFetch(`http://localhost:3333/api/levels`, {});
	console.log(error);
	console.log(loadingFetch);
	console.log(response);

	const loadLevels = async () => {
		const levels = await api.get("/levels");
		setItems(levels.data);
		setLoading(false);
	};

	const reloadLevel = () => {
		const ret = [...items];

		if (lastLevel) {
			var colShow = ret.reduce((t, n) => {
				if (n.number <= lastLevel) {
					n.show = true;
					n.last = false;
					t = n;
				} else {
					n.show = false;
					n.last = false;
				}

				return t;
			}, null);

			if (colShow) {
				ret.map((i) => {
					if (i.number === colShow.number) {
						i.last = true;
					}

					return i;
				});
			}
		}

		setItems(ret);
	};

	useEffect(() => {
		localStorage.removeItem("authrn-level");
		localStorage.removeItem("authrn-category");

		if (report && report._id && loading) {
			loadLevels();

			const lastNumber = getInfoLevel();
			setLastLevel(lastNumber);
		}
	}, [report, loading]);

	useEffect(() => {
		if (!loading) {
			reloadLevel();
		}
	}, [loading]);

	useEffect(() => {
		reloadLevel();
	}, [lastLevel]);

	const menos = () => {
		setLastLevel(lastLevel === 1 ? 1 : lastLevel - 1);
	};

	const mais = () => {
		setLastLevel(lastLevel === 10 ? 10 : lastLevel + 1);
	};

	const goTo = (level) => {
		history.push(`/level/${level._id}`);
	};

	return (
		<>
			<img src={mapa} alt="" width="500px" />

			{items.map((l, i) => {
				const myStyle = { visibility: l.show ? "visible" : "hidden" };

				return (
					<div key={l._id} className={`animate__animated levelPosition levelPosition_${i + 1} ${l.last ? "blink" : ""}`} style={myStyle}>
						<div className="point">
							<img src={seta} alt="" width="20px" />
						</div>

						<div onClick={() => goTo(l)} className="title">
							{l.title}
						</div>
					</div>
				);
			})}

			<button onClick={() => menos()}>-</button>
			<button onClick={() => mais()}>+</button>
		</>
	);
};

export default Mapa;
