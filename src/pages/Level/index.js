import React, { useState, useEffect } from "react";
import "./styles.css";
import api from "../../services/api";
import { useHistory } from "react-router-dom";
import { useReport } from "../../contexts/report";

const Level = ({ match }) => {
	const [level, setLevel] = useState({});
	const [categories, setCategories] = useState([]);
	const [loading, setLoading] = useState(true);
	const history = useHistory();
	const { getInfoCategory } = useReport();

	const getLevel = async (id) => {
		const { data } = await api.get(`/levels/${id}`);
		setLevel(data);
		getCategoriesByLevel(id);
	};

	const getCategoriesByLevel = async (id) => {
		const { data } = await api.get(`/categories/level/${id}`);
		setCategories(data);
		setLoading(false);
	};

	useEffect(() => {
		getLevel(match.params.id);
	}, [match.params.id]);

	useEffect(() => {
		if (categories.length > 0) {
			const lastCategory = getInfoCategory(level);

			categories.map((c) => {
				c.isAvailable = false;
				if (c.number <= lastCategory) {
					c.isAvailable = true;
				}

				return c;
			});
		}
	}, [categories]);

	const goTo = (level) => {
		if (level.isAvailable) {
			history.push(`/category/${level._id}`);
		}
	};

	return (
		<div className="container">
			<h2>{level.title}</h2>

			{loading ? (
				"Loading..."
			) : (
				<ul>
					{categories.map((c) => {
						return (
							<li key={c._id} onClick={() => goTo(c)} style={{ textDecoration: !c.isAvailable ? "line-through" : "none" }}>
								{c.title}
							</li>
						);
					})}
				</ul>
			)}
		</div>
	);
};

export default Level;
