import React, { useState, useEffect } from "react";
import "./styles.css";
import api from "../../services/api";
import { useReport } from "../../contexts/report";
import { useAuth } from "../../contexts/auth";
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";

const Category = ({ match }) => {
	const [category, setCategory] = useState({});
	const [question, setQuestion] = useState({});
	const [answers, setAnswers] = useState([]);
	const [loading, setLoading] = useState(true);
	const { user } = useAuth();
	const history = useHistory();
	const { report, getReport, setCurrentLevel, setCurrentCategory, currentLevel, currentCategory, getInfoQuestion } = useReport();

	const getCategory = async (id) => {
		const { data } = await api.get(`/categories/${id}`);
		setCategory(data);
		getQuestion(id);
	};

	const getQuestion = async (id) => {
		const lastQuestion = getInfoQuestion(id);

		const { data } = await api.get(`/questions/${lastQuestion.question._id}`);
		setQuestion(data);

		getAnswers(lastQuestion.question._id);
	};

	const getAnswers = async (question_id) => {
		const { data } = await api.get(`/answers/question/${question_id}`);
		setAnswers(data);
		setLoading(false);
	};

	useEffect(() => {
		getCategory(match.params.id);
	}, [match.params.id]);

	const setNextAnswer = async (question_id) => {
		setLoading(true);

		const { data } = await api.get(`/questions/${question_id}/category/${currentCategory._id}`);
		if (data.length >= 1) {
			const newQuestion = data[0];

			const lastLevel = report.levels.filter((l) => {
				if (Object.keys(l) == currentLevel.number) {
					return l;
				}
			})[0];

			const colCategories = lastLevel[currentLevel.number].category;
			let lastPos = {};

			Object.keys(colCategories[0]).filter((f) => {
				if (colCategories[0][f]._id === currentCategory._id) {
					lastPos = colCategories[0][f].question = {
						_id: newQuestion._id,
						title: newQuestion.title,
					};
				}
			});

			const nextReport = {
				...report,
				levels: [...report.levels],
			};

			setCurrentCategory(lastPos);
			localStorage.setItem("authrn-category", JSON.stringify(lastPos));

			await api.put(`/reports/${report._id}`, nextReport);
			await getReport(user._id);

			getCategory(match.params.id);
		} else {
			// chegou no ultimo, define o primeiro novamente
			history.push(`/level/${currentLevel._id}`);
		}
	};

	const verifyAnswer = (a) => {
		if (a.is_correct) {
			toast("Voce acertou", {
				position: toast.POSITION.BOTTOM_RIGHT,
				type: toast.TYPE.SUCCESS,
				onOpen: () => {
					setNextAnswer(currentCategory.question._id);
				},
			});
		} else {
			toast("Voce errou", {
				position: toast.POSITION.BOTTOM_RIGHT,
				type: toast.TYPE.ERROR,
			});
		}
	};

	return (
		<div className="container">
			<h2>{category.title}</h2>

			{loading ? (
				"Loading..."
			) : (
				<>
					<h2>{question.title}</h2>

					<ul>
						{answers.map((a) => {
							return (
								<li key={a._id} onClick={() => verifyAnswer(a)}>
									{a.title}
								</li>
							);
						})}
					</ul>
				</>
			)}
		</div>
	);
};

export default Category;
