import React, { useState, useEffect } from "react";
import "./styles.css";
import Mapa from "../Mapa";
import { useReport } from "../../contexts/report";
import { useAuth } from "../../contexts/auth";

const Dashboard = () => {
	const { report, getReport, loading } = useReport();
	const { user } = useAuth();

	useEffect(() => {
		if (!report) {
			getReport(user._id);
		}
	}, []);

	return <div className="container">{loading ? "Loading..." : <Mapa />}</div>;
};

export default Dashboard;
