import React, { useState, useEffect } from "react";
import api from "../../services/api";
import { useAuth } from "../../contexts/auth";
import ReactCardFlip from "react-card-flip";
import { GoogleLogin } from "react-google-login";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

import logo from "../../assets/logo.png";
import "./styles.css";

const Login = () => {
	const [form, setForm] = useState({ name: "", email: "admin@servidor.com", password: "admin" });
	const { login, newLogin, setLoading } = useAuth();

	const handleSignIn = (e) => {
		e.preventDefault();

		const { email, password } = form;
		login(email, password);
	};

	const handleChange = (e) => {
		setForm({
			...form,
			[e.target.name]: e.target.value,
		});
	};

	const responseGoogle = async (response) => {
		if (response.error) {
			return;
		}

		setLoading(true);
		const res = await api.post("/google/account", { profile: response });

		const { email, password } = res.data;
		login(email, password);
	};

	const responseFacebook = async (response) => {
		if (response.status === "unknown") {
			return;
		}

		setLoading(true);
		const res = await api.post("/facebook/account", { profile: { ...response, access_token: response.accessToken } });

		const { email, password } = res.data;
		login(email, password);
	};

	const [isFlipped, setFlipped] = useState(false);

	const handleSignup = (e) => {
		e.preventDefault();

		const { name, email, password } = form;
		newLogin(name, email, password);
	};

	const changeScreen = (e) => {
		e.preventDefault();
		setFlipped(!isFlipped);
	};

	return (
		<div className="contentLogin">
			<div className="content">
				<ReactCardFlip isFlipped={isFlipped}>
					<div role="button" className="myCardFront">
						<form onSubmit={handleSignIn}>
							<img className="logotipo" src={logo} alt="" />
							<div>
								<span>E-mail</span>
								<input value={form.email} name="email" onChange={(e) => handleChange(e)} />
							</div>
							<div>
								<span>Password</span>
								<input value={form.password} name="password" onChange={(e) => handleChange(e)} type="password" />
							</div>

							<div className="boxHelp">
								<button type="button" className="btnHelp" onClick={(e) => changeScreen(e)}>
									New account
								</button>

								<button type="submit" className="btnSign">
									Sign in
								</button>
							</div>
						</form>
					</div>

					<div role="button" className="myCardBack">
						<form onSubmit={handleSignup}>
							<img className="logotipo" src={logo} alt="" />
							<div>
								<span>Name</span>
								<input value={form.name} name="name" onChange={(e) => handleChange(e)} />
							</div>
							<div>
								<span>E-mail</span>
								<input value={form.email} name="email" onChange={(e) => handleChange(e)} />
							</div>
							<div>
								<span>Password</span>
								<input value={form.password} name="password" onChange={(e) => handleChange(e)} type="password" />
							</div>

							<div className="boxHelp">
								<button type="button" className="btnHelp" onClick={(e) => changeScreen(e)}>
									Back
								</button>

								<button type="submit" className="btnSign">
									Save
								</button>
							</div>
						</form>
					</div>
				</ReactCardFlip>

				<div className="boxOthersLogin">
					<GoogleLogin
						clientId="388443557131-9t7280vjf1fpvdr5til7qpg94oiig1gm.apps.googleusercontent.com"
						render={(renderProps) => (
							<button className="btnGoogle" onClick={renderProps.onClick} disabled={renderProps.disabled}>
								Sign in with <br /> Google
							</button>
						)}
						onSuccess={responseGoogle}
						onFailure={responseGoogle}
						cookiePolicy={"single_host_origin"}
					/>

					<FacebookLogin
						appId="3477704425594280"
						render={(renderProps) => (
							<button className="btnFacebook" onClick={renderProps.onClick}>
								Sign in with <br /> Facebook
							</button>
						)}
						autoLoad={false}
						fields="name,email"
						callback={responseFacebook}
						icon="fa-facebook"
					/>
				</div>
			</div>
		</div>
	);
};

export default Login;
