import React, { createContext, useContext, useState, useEffect } from "react";
import { createLogin, signIn, signOutGoogle, signOutFacebook } from "../services/auth";
import { toast } from "react-toastify";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
	const [user, setUser] = useState(null);
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		const storagedUser = localStorage.getItem("authrn-user");
		const storagedToken = localStorage.getItem("authrn-token");

		if (storagedUser && storagedToken) {
			setUser(JSON.parse(storagedUser));
		}

		setLoading(false);
	}, []);

	const newLogin = async (name, email, password) => {
		setLoading(true);
		const { data } = await createLogin(name, email, password);
		login(data.email, data.password);
	};

	const login = async (email, password) => {
		setLoading(true);
		const { data } = await signIn(email, password);

		if (data.error) {
			toast(data.error, {
				position: toast.POSITION.BOTTOM_RIGHT,
				type: toast.TYPE.ERROR,
			});

			setLoading(false);
			return;
		}

		const { token } = data;

		var base64 = token.split(".")[1];
		const jwtData = JSON.parse(window.atob(base64));

		const user = jwtData.item;
		setUser(user);

		localStorage.setItem("authrn-user", JSON.stringify(user));
		localStorage.setItem("authrn-token", token);
		setLoading(false);
	};

	const logout = async () => {
		setLoading(true);
		const { access_token } = user;

		if (access_token) {
			try {
				await signOutGoogle(access_token);
			} catch (e) {}

			try {
				await signOutFacebook(access_token);
			} catch (e) {}
		}

		localStorage.clear();
		setUser(null);
		setLoading(false);
	};

	return <AuthContext.Provider value={{ signed: !!user, user, loading, setLoading, login, logout, newLogin }}>{children}</AuthContext.Provider>;
};

export function useAuth() {
	const context = useContext(AuthContext);
	return context;
}
