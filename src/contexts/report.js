import React, { createContext, useContext, useState, useEffect } from "react";
import { loadReport, createReport } from "../services/auth";

const ReportContext = createContext();

export const ReportProvider = ({ children }) => {
	const [report, setReport] = useState(null);
	const [loading, setLoading] = useState(true);
	const [currentLevel, setCurrentLevel] = useState({});
	const [currentCategory, setCurrentCategory] = useState({});

	useEffect(() => {
		const storagedReport = localStorage.getItem("authrn-report");
		const storagedLevel = localStorage.getItem("authrn-level");
		const storagedCategory = localStorage.getItem("authrn-category");

		if (storagedReport) {
			setReport(JSON.parse(storagedReport));
		}

		if (storagedLevel) {
			setCurrentLevel(JSON.parse(storagedLevel));
		}

		if (storagedCategory) {
			setCurrentCategory(JSON.parse(storagedCategory));
		}

		setLoading(false);
	}, []);

	const getReport = async (user_id) => {
		const { data } = await loadReport(user_id);

		if (data && data.length > 0) {
			setReport(data[0]);
			localStorage.setItem("authrn-report", JSON.stringify(data[0]));
		}

		setLoading(false);
	};

	const getInfoLevel = () => {
		const levelCurrent = report.levels[report.levels.length - 1];
		const lastLevel = parseInt(Object.keys(levelCurrent)[0]);
		return lastLevel;
	};

	const getInfoCategory = (level) => {
		setCurrentLevel(level);
		localStorage.setItem("authrn-level", JSON.stringify(level));

		const positionLevel = level.number - 1;
		const lastLevel = parseInt(Object.keys(report.levels[positionLevel])[0]);
		const categoriesList = report.levels[positionLevel][lastLevel].category;

		const lastCat = categoriesList[categoriesList.length - 1];
		const lastPos = Object.keys(lastCat);
		const categoryCurrent = parseInt(lastPos[lastPos.length - 1]);

		return categoryCurrent;
	};

	const getInfoQuestion = (category) => {
		const lastLevel = report.levels.filter((l) => {
			if (Object.keys(l) == currentLevel.number) {
				return l;
			}
		})[0];

		const colCategories = lastLevel[currentLevel.number].category;
		let lastPos = {};

		Object.keys(colCategories[0]).filter((f) => {
			if (colCategories[0][f]._id === category) {
				lastPos = colCategories[0][f];
			}
		});

		setCurrentCategory(lastPos);
		localStorage.setItem("authrn-category", JSON.stringify(lastPos));

		return lastPos;
	};

	return <ReportContext.Provider value={{ report, loading, currentLevel, setCurrentLevel, setCurrentCategory, currentCategory, getInfoCategory, getInfoQuestion, getInfoLevel, getReport }}>{children}</ReportContext.Provider>;
};

export function useReport() {
	const context = useContext(ReportContext);
	return context;
}
