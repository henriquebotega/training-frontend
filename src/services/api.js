import axios from "axios";

const api = axios.create({
	baseURL: "http://localhost:3333/api",
	// baseURL: "https://train-ing-backend.herokuapp.com/api",
});

api.interceptors.request.use(
	function (config) {
		config.headers["x-access-token"] = localStorage.getItem("authrn-token");
		return config;
	},
	function (error) {
		return Promise.reject(error);
	}
);

api.interceptors.response.use(
	function (response) {
		return response;
	},
	function (error) {
		if (error.response && error.response.status === 401) {
			localStorage.clear();
			window.location.href = "/login";
		}

		if (!error.response.data.auth) {
			return error.response;
		}

		return Promise.reject(error);
	}
);

export default api;
