import api from "./api";
import axios from "axios";

export const createReport = async (user_id) => {
	try {
		return await api.post("/reports", { user_id });
	} catch (error) {
		return error;
	}
};

export const loadReport = async (user_id) => {
	try {
		return await api.get(`/reports/user_id/${user_id}`);
	} catch (error) {
		return error;
	}
};

export const createLogin = async (name, email, password) => {
	try {
		return await api.post("/users", { name, email, password });
	} catch (error) {
		return error;
	}
};

export const signIn = async (email, password) => {
	try {
		return await api.post("/login", { email, password });
	} catch (error) {
		return error;
	}
};

export const signOutGoogle = async (access_token) => {
	var fData = new FormData();
	fData.set("xsrfToken", "");
	fData.set("token", access_token);

	await axios({
		method: "post",
		url: "https://accounts.google.com/o/oauth2/revoke",
		data: fData,
		headers: { "Content-Type": "multipart/form-data" },
	});
};

export const signOutFacebook = async (access_token) => {
	await axios({
		method: "get",
		url: "https://www.facebook.com/x/oauth/logout?access_token=" + access_token,
		headers: { "Content-Type": "application/json" },
	});
};
