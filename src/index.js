import React from "react";
import ReactDOM from "react-dom";
import Routes from "./routes";
import { AuthProvider } from "./contexts/auth";
import { ReportProvider } from "./contexts/report";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import "./global.css";

ReactDOM.render(
	<React.StrictMode>
		<AuthProvider>
			<ReportProvider>
				<Routes />
				<ToastContainer />
			</ReportProvider>
		</AuthProvider>
	</React.StrictMode>,
	document.getElementById("root")
);
